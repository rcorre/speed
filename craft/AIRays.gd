extends Spatial

onready var left: RayCast = $LeftRay
onready var right: RayCast = $RightRay
onready var craft: Craft = get_parent()

func _ready():
	if not craft.local_player:
		# AI (or controlling player after finish
		left.enabled = true
		right.enabled = true

	# Let AI Take over after player finishes
	craft.connect("finished", left, "set_enabled", [true])
	craft.connect("finished", right, "set_enabled", [true])

func _collision_depth(r: RayCast) -> float:
	if not r.is_colliding():
		return 1.0
	var length := global_transform.origin.distance_to(r.get_collision_point())
	return length * 3.0 / r.cast_to.length()

func _physics_process(_delta: float):
	if craft.local_player and not craft.finished:
		return
	craft.input_accel = 1.0
	craft.input_brake = 0.0
	craft.input_boost = 0.0
	craft.input_turn = _collision_depth(left) - _collision_depth(right)
