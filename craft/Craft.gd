extends KinematicBody
class_name Craft

const MAX_SPEED := 75.0
const MAX_SPEED_VARIANCE := 30.0
const ACCEL := 50.0
const ACCEL_VARIANCE := 20.0
const BRAKE := 80.0
const MAX_TURN := 4.0
const TURN_RATE := PI / 2
const TURN_VARIANCE := PI / 2
const GRAVITY := 20.0
const GRIP := 3.0
const GRIP_VARIANCE := 2.0
const BOOST_SPEED := 50.0
const BOOST_ACCEL := 25.0
const BOOST_COST := 0.2

const STATS = {
	"QuickSilver": {
		"grip": 0.5 ,
		"accel": 0.6,
		"speed": 0.5,
		"turn": 0.7 ,
	},
	"Javelin": {
		"grip": 0.9 ,
		"accel": 0.7,
		"speed": 0.7,
		"turn": 0.2 ,
	},
	"Goliath": {
		"grip": 0.4 ,
		"accel": 0.3,
		"speed": 1.0,
		"turn": 0.5 ,
	},
	"Zephyr": {
		"grip": 0.2 ,
		"accel": 0.9,
		"speed": 0.3,
		"turn": 0.9 ,
	},
}

signal finished

export(bool) var local_player := false

var craft_name := "QuickSilver"
var velocity := Vector3.ZERO
var power := 1.0

var input_turn := 0.0
var input_accel := 0.0
var input_brake := 0.0
var input_boost := 0.0
var current_lap := 1
var finished := false

func get_stat(name: String) -> float:
	if not local_player:
		# hack: AI always uses the same stats
		return 0.0
	# stats use 0.5 as the "baseline"
	# < 0.5 is a penalty, > 0.5 is a bonus
	return STATS[craft_name][name] - 0.5

func max_speed() -> float:
	return MAX_SPEED + get_stat("speed") * MAX_SPEED_VARIANCE

func accel() -> float:
	return ACCEL + get_stat("accel") * ACCEL_VARIANCE

func turn_rate() -> float:
	return TURN_RATE + get_stat("turn") * TURN_VARIANCE

func grip() -> float:
	return GRIP + get_stat("grip") * GRIP_VARIANCE

func speed_factor() -> float:
	# TODO: translate velocity to local
	return abs(global_transform.basis.xform_inv(velocity).z / MAX_SPEED)

func grip_factor() -> float:
	return lerp(1.0, grip(), speed_factor())

func signed_angle(v1: Vector3, v2: Vector3, axis: Vector3) -> float:
	var cross_to := v1.cross(v2);
	var unsigned_angle := atan2(cross_to.length(), v1.dot(v2));
	var signed := cross_to.dot(axis);
	return unsigned_angle * sign(signed)

# 0 is no drift
# -1 is full drift left
# 1 is full drift right
func drift_factor() -> float:
	if velocity.length() <= 5.0:
		return 0.0
	return signed_angle(velocity, global_transform.basis.z, global_transform.basis.y)

func _enter_tree():
	add_to_group("craft")

func _ready():
	if local_player:
		propagate_call("select_craft", [Global.player_craft])
	else:
		var names := STATS.keys()
		propagate_call("select_craft", [names[randi() % len(names)]])

func select_craft(n: String):
	craft_name = n

func _unhandled_input(ev: InputEvent):
	if not finished:
		return
	if (ev.is_action("Boost") or ev.is_action("ui_accept")) and ev.is_pressed():
		get_tree().change_scene("res://menus/MainMenu.tscn")

func _physics_process(delta: float):
	if local_player and not finished:
		input_turn = (
			Input.get_action_strength("Turn Left") -
			Input.get_action_strength("Turn Right")
		)
		input_accel = Input.get_action_strength("Accelerate")
		input_brake = Input.get_action_strength("Brake")
		input_boost = Input.get_action_strength("Boost") and power > 0

	var turn := input_turn * turn_rate() * delta
	rotate_y(turn)

	var facing := global_transform.basis.z.normalized()

	# bring velocity into alignment with facing proportional to grip
	# high grip means velocity quickly alings to facing
	# low grip means spending longer "sliding" in a different direction than the facing
	var target_vel := facing * velocity.length()
	velocity = velocity.linear_interpolate(target_vel, grip_factor() * delta)

	# now accelerate towards the speed input
	if input_boost:
		power = max(0, power - delta * BOOST_COST)

	var desired_vel := facing * (input_accel * max_speed() + BOOST_SPEED * input_boost)
	var accel := accel() * (1 - speed_factor()) + input_boost * BOOST_ACCEL
	velocity = velocity.move_toward(desired_vel, accel * delta)
	velocity -= facing * input_brake * BRAKE * speed_factor() * delta
	velocity.y -= GRAVITY * delta

	var snap := Vector3.DOWN * 5
	velocity = move_and_slide_with_snap(velocity, snap, Vector3.UP)

func get_course_progress() -> float:
	var course_path: Path = get_tree().get_nodes_in_group("course_path")[0]
	return course_path.curve.get_closest_offset(
		course_path.to_local(global_transform.origin)
	)

func get_place() -> int:
	var progress = get_course_progress()
	var nodes := get_tree().get_nodes_in_group("craft")
	var rank := len(nodes)
	for n in nodes:
		var c: Craft = n
		if current_lap > c.current_lap or progress > c.get_course_progress():
			rank -= 1
	return rank

func new_lap():
	current_lap += 1

func finish():
	emit_signal("finished")
	finished = true
