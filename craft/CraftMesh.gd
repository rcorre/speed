extends MeshInstance

func select_craft(craft_name: String):
	visible = name == craft_name
