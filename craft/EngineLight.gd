extends MeshInstance

export(Vector3) var speed_scale := Vector3(0.0, 1.0, 5.0)

onready var craft: Craft = owner

func _process(delta: float):
	var target_scale := (
		Vector3.ONE
		- craft.input_brake * Vector3.ONE
		+ speed_scale * craft.input_accel 
		+ speed_scale * craft.input_boost
	)
	scale = lerp(scale, target_scale, 10 * delta)
