extends Label

func _process(_delta: float):
	var craft: Craft = owner
	var place := craft.get_place()
	match place:
		1:
			text = "1st"
		2:
			text = "2nd"
		3:
			text = "3rd"
		_:
			text = "%dth" % place
