extends RayCast

onready var particles: Particles = $Particles
onready var sound: AudioStreamPlayer3D = $Sound

func _physics_process(_delta: float):
	particles.emitting = is_colliding()
	sound.stream_paused = not is_colliding()
