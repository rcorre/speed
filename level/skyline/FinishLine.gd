extends Area

export(int, 0, 10) var max_laps := 5

onready var label: Label = $Viewport/CenterContainer/Label

var current_lap := 1

func _ready():
	connect("body_entered", self, "_on_body_entered")
	label.set_text("Lap 1/%d" % max_laps)

func _on_body_entered(craft: Craft):
	craft.new_lap()
	if craft.current_lap > current_lap:
		current_lap = craft.current_lap
		if current_lap > max_laps:
			craft.finish()
		elif current_lap == max_laps:
			label.set_text("Finish!")
		else:
			label.set_text("Lap %d/%d" % [current_lap, max_laps])
