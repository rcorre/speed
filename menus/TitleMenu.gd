extends VBoxContainer

export(PackedScene) var next_scene: PackedScene

func _unhandled_input(ev: InputEvent):
	if ev is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		return
	if not (visible and ev.is_pressed()):
		return
	if (ev is InputEventKey or ev is InputEventJoypadButton):
		var tabs: TabContainer = get_parent()
		tabs.current_tab += 1
		get_tree().set_input_as_handled()
