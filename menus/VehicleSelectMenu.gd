extends Control

export(PackedScene) var next_scene: PackedScene

var craft_names := [
	"QuickSilver",
	"Javelin",
	"Goliath",
	"Zephyr",
]

var craft_idx := 0

func _unhandled_input(ev: InputEvent):
	if not (visible and ev.is_pressed()):
		return
	if ev.is_action("Turn Right") or ev.is_action("ui_right"):
		next_vehicle()
	elif ev.is_action("Turn Left") or ev.is_action("ui_left"):
		prev_vehicle()
	elif ev.is_action("Boost"):
		Global.player_craft = craft_names[craft_idx]
		get_tree().change_scene_to(next_scene)

func _ready():
	propagate_call("select_craft", [craft_names[craft_idx]])

func next_vehicle():
	craft_idx = (craft_idx + 1) % len(craft_names)
	propagate_call("select_craft", [craft_names[craft_idx]])

func prev_vehicle():
	craft_idx = (craft_idx - 1) % len(craft_names)
	propagate_call("select_craft", [craft_names[craft_idx]])
