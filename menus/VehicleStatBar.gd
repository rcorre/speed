extends ProgressBar

export(String, "", "speed", "accel", "turn", "grip") var stat: String

var target_val := 0.0

func select_craft(craft_name: String):
	target_val = Craft.STATS[craft_name][stat]

func _process(_delta: float):
	value = lerp(value, target_val, 0.2)
